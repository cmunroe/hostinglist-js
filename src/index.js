"use strict";
const r = require('axios');
var list;

function update(){
    return new Promise((resolve, reject) => {
        const blackbox = r.get('https://raw.githubusercontent.com/Munzy/blackbox/master/blocks/hosting.csv');
        Promise.all([blackbox])
        .then(values => {
            var data;
            values.forEach(value => {
                data += value.data;

            });
            return data;
        })
        .then(values => {
            return values.split("\n")
        })
        .then(values => {

            let data = []
            values.forEach(value => {
                let asn = parseInt(value.split(",")[0])
                if(!isNaN(asn)){data.push(asn)}
            })
            return data

        })
        .then(values => {
            list = values;
            resolve();
        })
        .catch(errors => {
            reject(errors)
        })
    })
}

function check(asn){
    asn = parseInt(asn)
    if(list){
        return list.includes(asn);
    }
    else{
        return null;
    }
}


update();
setInterval(update, 1000 * 60 * 60 * 24);
module.exports = {check, update};